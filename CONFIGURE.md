# Part 2: Configuration

A fresh Debian install is very minimal. My first steps were to install some goodies. Since sudo isn't even installed, I had to use `su` to do anything. really.

```bash
su
apt update && apt upgrade
apt install sudo curl git
```

## Sudo

You can turn off password for sudo if you like:

```bash
visudo
```

Then add a line for your user below `root`.

```
# ... other config here ...

# User privilege specification
root    ALL=(ALL:ALL) ALL
yourusername    ALL=NOPASSWD: ALL

# ... other config here ...
```


## Avahi

This is some great software that can help your networked computers broadcast their hostname.

```zsh
sudo apt-get install avahi-daemon
```


## Shell Preference

Since this is going to be a command-line only interface, we want to hack in style. Install your favorite shell. I highly recommend zsh.

```bash
# Install zsh
sudo apt install zsh

# Install Oh My Zsh!
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install spaceship theme for Oh My Zsh!
# This is obviously optional but I like this theme.
curl -o - https://raw.githubusercontent.com/denysdovhan/spaceship-prompt/master/scripts/install.sh | zsh
```


## SMB Shares

SMB (also Samba or CIFS) is compatible with Mac, PC, and Linux. It's generally as fast or faster than alternatives such as NFS. You might say it has a decent compatability-to-optimality ratio when it comes to choosing a protocol.

```bash
# Install samba
sudo apt install samba

# Create a samba user
sudo smbpasswd -a <user_name>

# Create a folder to be shared
mkdir /home/<user_name>/<folder_name>

# Backup the smb conf
sudo cp /etc/samba/smb.conf ~
```

Edit `/smb/samba/smb.conf`, adding a section to the bottom:

```
[<folder_name>]
path = /home/<user_name>/<folder_name>
valid users = <user_name>
read only = no
```

Restart samba with `/etc/init.d/smbd restart`.

See [this guide](https://help.ubuntu.com/community/How%20to%20Create%20a%20Network%20Share%20Via%20Samba%20Via%20CLI%20%28Command-line%20interface/Linux%20Terminal%29%20-%20Uncomplicated%2C%20Simple%20and%20Brief%20Way%21) for more information.

Once you have the share set-up on the server , you can [permanently mount it on your workstation](https://wiki.ubuntu.com/MountWindowsSharesPermanently).


## Set-up HFS+ Volume

I have some USB drives from my Mac days that I also plugged-into the server. In case you want to use HFS+, here's some quick info how to do that.

First install the packages for HFS+:

```bash
sudo apt-get install hfsplus hfsutils hfsprogs
```

Next [get the UUID of your disk](https://liquidat.wordpress.com/2007/10/15/short-tip-get-uuid-of-hard-disks/). Again use `sudo fdisk -l` to list all of your disks.

```bash
ls -l /dev/disk/by-uuid
```

Make sure you have a folder to mount this to, e.g. `sudo mkdir -p /media/usb`. Finally, update `/etc/fstab` with a line similar to what I have below. Change `12345` to your UUID from the step above and change `/media/usb` to the mount path you chose.

```fstab
UUID=12345 /media/usb hfsplus force,rw,default 0 0
```


## S.M.A.R.T. Drive Monitoring

This will allow us to receive an alert when a drive goes bad. In the commands below, be sure to replace `/dev/sda` with the drive(s) you are interested in. You can use `fdisk -l` to list the drives available to you.

First let's just do a one-off test to see what SMART status looks like and verify that everything is functioning properly.

```bash
# Install software
sudo apt install smartmontools

# Check to make sure your drive supports SMART
sudo smartctl -i /dev/sda | grep SMART

# In case it's not enabled, you can enable it
# sudo smartctl -s on /dev/sda

# Check how long the short self-test is going to take (mine is 2 minutes)
# sudo smartctl -c /dev/sda

# Run a short test
sudo smartctl -t short /dev/sda

# Check the results after 2 min or so
sudo smartctl -H /dev/sda
```

We can use a SMART test regularly via `[smartd](https://www.smartmontools.org/browser/trunk/smartmontools/smartd.conf.5.in)`.
