#!/bin/bash
mkdir loopdir
mount -o loop debian.iso loopdir
mkdir -p cd
rsync -a -H --exclude=TRANS.TBL loopdir/ cd
umount loopdir
rmdir loopdir

mkdir irmod
cd irmod
gzip -d < ../cd/install.amd/initrd.gz | \
  cpio --extract --verbose --make-directories --no-absolute-filenames
cp ../my_preseed.cfg preseed.cfg
find . | cpio -H newc --create --verbose | \
  gzip -9 > ../cd/install.amd/initrd.gz
cd ../
rm -fr irmod/

cd cd
cp ../my_txt.cfg isolinux/txt.cfg
cp ../my_preseed.cfg preseed.cfg
md5sum `find -follow -type f` > md5sum.txt
cd ..

# CD Only:
# genisoimage -o debian-headless.iso -r -J -no-emul-boot -boot-load-size 4 \
# -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./cd

# CD Only Alt:
# mkisofs -o debian-headless.iso -r -J -no-emul-boot -boot-load-size 4 -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./cd

# CD or USB:
# Note you may need to apt install isolinux xorriso
xorriso -as mkisofs -o debian-headless.iso \
 -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
 -c isolinux/boot.cat -b isolinux/isolinux.bin \
 -no-emul-boot -boot-load-size 4 -boot-info-table ./cd

# Optional test:
# qemu-system-x86_64 -net user -cdrom debian-headless.iso