# Headless Debian Install

<small>*Published January 1st, 2018.*</small>

"Headless" means without a monitor.

In this guide we will create a custom bootable USB thumbdrive via Linux that will allow us to perform a remote installation of Debian 9.

Required materials:

* Workstation (ideally running Linux)
* Headless server
* USB thumbdrive

Please ensure that **all data is backed-up** before following this guide. The entire hard drive of the headless server and your USB stick will be erased. Also a disclaimer: You may not hold me responsible for anything that happens as a result of following this guide. Proceed **AT YOUR OWN RISK** and be sure to take necessary care and precaution.


## Why?

I have an old [Acer EasyStore H340](https://www.newegg.com/Product/Product.aspx?Item=N82E16859321013) from 2010. You literally cannot attach a monitor to this thing, but it's begging for Linux and will make a mean RAID 5 NAS with 16 TB for under $1000.

Installing Debian on this server was much more difficult than I expected and it required a lot of reading and rooting through outdated articles, trial and error, and tinkering with configuration directives. I hope this repo will help someone avoid all the headaches I encountered.


## How to use this guide

This gude is designed for folks with at least intermediate skill level using command line interfaces and performing computer maintenance. I did my best to write down *exactly* what I did without skipping steps, though your situation may differ from mine.

Your downloads, configuration, and commands may vary slightly depending on the architecture of your machine(s), etc. Read carefully and modify as needed.

If your workstation is not running Linux, (e.g. macOS or Windows), many of the instructions **may still work** (though I haven't tested it!). On macOS, use Terminal.app to run commands and [`brew`](http://brew.sh/) to install missing dependencies (instead of `apt-get`). On Windows 10+, be sure to [enable the Linux subsystem](https://docs.microsoft.com/en-us/windows/wsl/install-win10). If you run into trouble, you can try executing the instructions in a Linux VM, e.g. via [VirtualBox](https://www.virtualbox.org/wiki/Downloads) or finding an equivalent solution for your environment.


## 1. Download Debian Installer

Debian is... pretty badass. The installer can simply be [copied to a thumbdrive verbatim](https://www.debian.org/releases/jessie/amd64/ch04s03.html.en#usb-copy-easy). However, in order to do a headless install, we will need to modify the installer iso first.

It's a good idea to have [the official installation guide](https://www.debian.org/releases/jessie/installmanual) handy. It'll help you [download the right iso](https://www.debian.org/distrib/ftplist).

My machine is a 64-bit x86 processor, so I downloaded the [amd64 cd installer iso](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/). Mine is an Intel Atom processor but the architecture is [still called amd64 for historical reasons](https://en.wikipedia.org/wiki/X86-64#cite_note-11).

**This part is important.** When you have the correct iso downloaded, move it to the same folder as this README.md file and rename it to `debian.iso`.


## 2. Configure Preseed

The way Debian enables headless installation is through a "[preseed](https://wiki.debian.org/DebianInstaller/Preseed#Preseeding_d-i)". This configuration file allows you to pre-select options for the installer and even complete an entire installation without any user input.

Simply make any changes to the `my_preseed.cfg` file included in this repo.

For a complete list of options see [Appendix B: Automating the installation using preseeding](https://www.debian.org/releases/stable/i386/apb.html) and the [preseed example](https://www.debian.org/releases/stretch/example-preseed.txt). Definitely take a look `d-i passwd/username` as this is the username you will use to login in step #7. Also, make sure you leave the root user enabled unless you figure out how to pre-install sudo.

You also want to take a look at any network configuration that may be specific to your scenario. My server was hard-wired into a router so the auto-conf feature worked beautifully. If you have any special security, static routing, or WiFi, you will need to configure that.

The main thing to note with my included `my_preseed.cfg` is it enables the [network console](https://wiki.debian.org/DebianInstaller/NetworkConsole) for installation. We set a cleartext password for the `installer` user and that enables us to complete the install over SSH.


## 3. Repack the ISO

A script is included to repack the ISO. It depends on [isolinux](http://www.syslinux.org/wiki/index.php?title=ISOLINUX) and [xorriso](https://www.gnu.org/software/xorriso/) to create a bootable USB image, so make sure those are installed:

```bash
sudo apt install isolinux xorriso
```

Then run `repack.sh` **as root**:

```bash
chmod +x ./repack.sh
sudo ./repack.sh
```

The final file will be output as `debian-headless.iso`.

If you want more information on this step, you can consult the [Debian Edit ISO wiki page](https://wiki.debian.org/DebianInstaller/Preseed/EditIso).


### Optionally Test the Repack

In some cases you may wish to test your configuration changes. I found the easiest way to do this was via either VirtualBox or `qemu`.

#### Using VirtualBox

1. Create a new VM *without* a virtual hard disk
2. Go to settings on that new VM and remove the Controller IDE entry
3. Add a new Controller IDE entry pointing to `debian-headless.iso`
4. Boot the VM

#### Using qemu

Simply run a command like the one below from within this repo folder. You may need to change the `-system-x86_64` part to match the architecture of your headless server.

```bash
qemu-system-x86_64 -net user -cdrom debian-headless.iso
```


## 4. Flash a USB Drive

The easiest way to do this with almost any OS is [Etcher](https://etcher.io/). For Linux it's an AppImage, so you just execute the file once you extract it.

Select `debian-headless.iso` from the GUI and flash your USB thumbdrive with it. Keep in mind this will **erase any data on your thumbdrive**, so make sure you back-up your data.

Flashing the drive only takes a couple minutes tops.


## 5. Plug it in!

Plug-in the USB drive to your headless server and boot it up! For my server I just unplugged the HDD until it boot from the USB, then I plugged-in the HDD. You may need to tinker with boot loader config or similar if this is not easy to do for you.

With the default configuration included in this guide, I had to hit enter one time on a keyboard once I imagined the installer was loaded. This opens the SSH server so you can connect via your workstation.

> **Tip** this process may take several minutes of waiting and wondering what's happening. Give it time to load up before you decide something is wrong.


## 6. Remote Install

Now the fun part. You have to first figure out which IP address is your headless server. I did this by logging-into my router web admin and looking under "Connected Hosts". You can also use `nmap` or similar software to [find live hosts on your LAN](https://security.stackexchange.com/questions/36198/how-to-find-live-hosts-on-my-network). Here is an example nmap command that will find hosts running ssh on the 10.0.0.x address range:

```bash
sudo nmap -PS -p22 10.0.0.1/24
```

Once you have the IP address, ssh to it like so (replace the IP with your headless server IP). The username and password is: **installer** / **r00tme** unless you changed it in the preseed conf.

> **Tip** you may wish to do this in a `screen` so it can be resumed in case you accidentally kill your terminal emulator.

```bash
ssh installer@10.0.0.69
```

If successful, you should see an installer interface. Follow the instructions and wait forever as things are copied from the USB stick onto your hard drive. With USB 2.0 and 5400 RPM drives, this basically took all night for me. I went to bed and finished the install in the morning.

Make sure you install the SSH server on the "Install Software" step. Down-arrow to SSH server in the list and press `SPACE` to select it from the list. `ENTER` to continue installing. You can "Go Back" at the end of the install if you miss something.


## 7. Login

Once installation completes, you can tell Debian installer you are ready to reboot and remove the installation media. The headless server should boot into Debian with SSH running!

Here is the command I used. Your username and password were configured in the preseed file. If you didn't change them, the login and password are **debian** / **changeme**. Once you're in, you can change the password with `passwd`. Again, replace the IP address with the one we found in step #6.

```bash
ssh debian@10.0.0.69
```


## Conclusion

Hopefully you saved time by learning from my mistakes and using my guide to perform a remote headless installation of Debian. For more information on how to set-up the server as a NAS, go to [Part 2: Configuration](CONFIGURE.md).
