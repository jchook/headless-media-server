# Repacking Debian for SSH Headless install

[Link](https://wiki.debian.org/DebianInstaller/Preseed/EditIso)

## Create copy of an install image.

	As root...

	```bash
	mkdir loopdir
	mount -o loop debian-9.3.0-amd64-netinst.iso loopdir
	mkdir cd
	rsync -a -H --exclude=TRANS.TBL loopdir/ cd
	umount loopdir
	```

## Hack the initrd

	As root...

	```bash
	mkdir irmod
	cd irmod
	gzip -d < ../cd/install.amd/initrd.gz | \
	        cpio --extract --verbose --make-directories --no-absolute-filenames
	cp ../my_preseed.cfg preseed.cfg
	find . | cpio -H newc --create --verbose | \
	        gzip -9 > ../cd/install.amd/initrd.gz
	cd ../
	rm -fr irmod/
	```

	Then fix the MD5

	```bash
	cd cd
	md5sum `find -follow -type f` > md5sum.txt
	cd ..
	```

	Then roll an image

	```bash
	genisoimage -o test.iso -r -J -no-emul-boot -boot-load-size 4 \
 -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./cd

 # Optional test
 qemu-system-x86_64 -net user -cdrom test.iso
 ```
